# encoding: utf-8

from __future__ import unicode_literals

from django import forms
from django.forms import modelformset_factory
from django.contrib.auth import authenticate, login

from ..models.post import Post, Tag


class CreatePostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'body', 'pub_date', 'tags']