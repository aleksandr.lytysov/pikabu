# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.db import models
from django.utils import timezone

# Create your models here.


class Tag(models.Model):
    title = models.CharField(max_length=20)

    def __str__(self):
        return self.title


class Post(models.Model):
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Date published', default= timezone.now())
    body = models.TextField('Content')
    rating = models.IntegerField('Carma', default=0)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return self.title

    def was_published_today(self):
        return self.calculate_time_delta(self, delta=1)

    def was_published_this_week(self):
        return self.calculate_time_delta(self, delta=7)

    def was_published_this_month(self):
        return self.calculate_time_delta(self, delta=30)

    def calculate_time_delta(self, delta):
        now = timezone.now()
        return now - datetime.timedelta(days=delta) <= self.pub_date <= now


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.CharField(max_length=200, default='Etot post govno')
    rating = models.IntegerField(default=0)

    def __str__(self):
        return self.text
