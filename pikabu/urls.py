# encoding: utf-8

from __future__ import unicode_literals
from django.conf.urls import url, include

from .views import *

app_name = 'pikabu'
urlpatterns = [
    url(r'^$', HomePageView.as_view(), name='main'),
    url(r'^posts/create/$', CreatePostView.as_view(), name='create_post'),
]
