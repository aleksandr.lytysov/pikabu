# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, request, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import ListView, CreateView

from ..models.post import Post, Comment, Tag
from ..forms import CreatePostForm

class HomePageView(ListView):
    template_name = 'index.html'
    context_object_name = 'posts'

    def get_queryset(self):
        result = Post.objects.order_by('-pub_date')[:50]
        return result

class CreatePostView(CreateView):
    template_name = "create_post.html"
    fields = ['title', 'body', 'tags']
    success_url = '/'

    def form_valid(self, form):
        title = form.cleaned_data['title']
        tags = form.cleaned_data['tags']
        post = Post(title=title)
        post.save()
        tag_list = Tag.objects.filter(pk__in=tags)
        for tag in tag_list:
            post.tags.add(tag)
        return HttpResponseRedirect('/pikabu')

    def create_post(self):
        form = CreatePostForm
        render(request, self.template_name, {'form': form})

    #ToDo remove this function
    def get_queryset(self):
        result = Post.objects.order_by('-pub_date')[:50]
        return result
