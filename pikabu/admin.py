# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models.post import Post, Comment, Tag

# Register your models here.


class CommentInline(admin.TabularInline):
    model = Comment
    extra = 3

class PostAdmin(admin.ModelAdmin):
    inlines = [CommentInline]

admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
admin.site.register(Tag)